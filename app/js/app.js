/* ==========================================================================
   SolGod
   ========================================================================== */

solGod.app = function(undefined) {

    var exports = this.app;


    window.globGrow = false;
    window.globTest = false;

    window.globMinZoom = 50;
    window.globMaxZoom = 10000;


    // On load
    exports.onload = function() {};


    // Init
    var init = function() {
        solGod.get();
        solGod.merge();
        solGod.save();

        solGod.sol();
    }();
};


var ready = function(fn) {
    // Sanity check
    if (typeof(fn) !== 'function') return;

    // If document is already loaded, run method
    if (document.readyState === 'complete') {
        return fn();
    }

    // Otherwise, wait until document is loaded
    document.addEventListener('DOMContentLoaded', fn, false);
};

ready(function() {
    solGod.app();
});


window.onload = function() {
    solGod.app.onload();
};
