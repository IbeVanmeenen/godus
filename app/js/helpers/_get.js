/* ==========================================================================
   SolGod - Get
   ========================================================================== */

solGod.get = function(undefined) {

    var exports = this.get;

    // Random Integer
    exports.randomInt = function(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    };


    // Degree
    exports.degree = function(radian) {
        return radian / Math.PI * 180;
    };


    // Radian
    exports.radian = function(degrees) {
        return degrees * Math.PI / 180;
    };


    // Point Sphere
    exports.pointSphere = function(rad1, rad2, r) {
        var x = Math.cos(rad1) * Math.cos(rad2) * r;
        var z = Math.cos(rad1) * Math.sin(rad2) * r;
        var y = Math.sin(rad1) * r;
        return [x, y, z];
    };


    // Land Color
    exports.landColor = function() {
        var landColor,
            colorParam = exports.randomInt(0, 100);

        if (colorParam < 10) {
            landColor = '#f1404b';
        } else if (colorParam < 20) {
            // Rust in the rocks on the surface
            landColor = '#ff5f2e';
        } else if (colorParam < 40) {
            // Sulfur
            landColor = '#f9c00c';
        } else if (colorParam < 60) {
            // Green life
            landColor = '#aacd6e';
        } else {
            // ICE planet
            landColor = '#ffffff';
        }

        return landColor;
    };


    // Color array
    var hex = function(c) {
        var s = '0123456789abcdef',
            i = parseInt (c);

        if (i === 0 || isNaN (c)) {
            return '00';
        }

        i = Math.round (Math.min (Math.max (0, i), 255));

        return s.charAt ((i - i % 16) / 16) + s.charAt (i % 16);
    };

    var convertToHex = function(rgb) {
        return new THREE.Color('#' + hex(rgb[0]) + hex(rgb[1]) + hex(rgb[2]));
    };

    var trim = function(s) {
        return (s.charAt(0) == '#') ? s.substring(1, 7) : s;
    };

    var convertToRGB = function(hex) {
        var color = [];

        color[0] = parseInt ((trim(hex)).substring (0, 2), 16);
        color[1] = parseInt ((trim(hex)).substring (2, 4), 16);
        color[2] = parseInt ((trim(hex)).substring (4, 6), 16);

        return color;
    };

    exports.colorArray = function(colorStart, colorEnd, colorCount) {
        // The beginning of your gradient
        var start = convertToRGB(colorEnd),
            end = convertToRGB(colorStart),
            len = colorCount,
            alpha = 0.0,
            saida = [];

        var i;

        for (i = 0; i < len; i++) {
            var c = [];
            alpha += (1.0/len);

            c[0] = start[0] * alpha + (1 - alpha) * end[0];
            c[1] = start[1] * alpha + (1 - alpha) * end[1];
            c[2] = start[2] * alpha + (1 - alpha) * end[2];

            saida.push(convertToHex(c));
        }

        return saida;
    };
};
