solGod.save = function(undefined) {

    var exports = this.save;

    var name = 'solGod';
    var version = 1;

    var saveBtn, clearBtn;
    var hasClear = false;

    var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
    var database;

    var savedData;



    // Setup save
    exports.setupSave = function(ageEl) {
        saveBtn.classList.remove('actions__item--hidden');

        saveBtn.addEventListener('click', function() {
            var btn = this;

            btn.disabled = true;
            btn.classList.add('actions__item--progress');

            var age = ageEl.innerHTML,
                planet = solGod.planet.getPlanet();

            var data = {
                metadata: {},
                age: age,
                planet: JSON.stringify(planet)
            };

            set(data).then(function() {
                btn.disabled = false;
                btn.classList.remove('actions__item--progress');

                if (!hasClear) {
                    setupClear();
                }

                clearBtn.classList.remove('actions__item--hidden');
            }, function(err) {
                console.warn(err);
                btn.disabled = false;
            });
        });
    };



    // Setup clear
    var setupClear = function() {
        hasClear = true;

        clearBtn.addEventListener('click', function() {
            var btn = this;

            btn.disabled = true;

            clear().then(function() {
                clearBtn.classList.add('actions__item--hidden');
                btn.disabled = false;
            }, function() {
                btn.disabled = false;
            });
        });
    };



    // Get saved data
    exports.getData = function() {
        return new Promise(function(resolve, reject) {
            // Already fetched?
            console.log(savedData);
            if (savedData !== undefined) {
                resolve(savedData);
            } else {
                get().then(function(data) {
                    if (data) {
                        // Store
                        savedData = data;

                        // Setup clear
                        setupClear();
                        clearBtn.classList.remove('actions__item--hidden');

                        // Resolve
                        resolve(data);
                    } else {
                        reject();
                    }
                }, function(err) {
                    console.warn(err);
                    reject();
                });
            }
        });
    };



    // open
    var open = function() {
        return new Promise(function(resolve, reject) {
            console.log('open');
            // Open indexedDB
            var request = indexedDB.open(name, version);

            request.onsuccess = function(e) {
                database = e.target.result;
                resolve();
            };

            request.onerror = function(e) {
                console.warn('IndexedDB', e);
                reject(e);
            };
        });
    };



    // Get
    var get = function() {
        return new Promise(function(resolve, reject) {
            console.log('get');
            open().then(function() {
                if (database.objectStoreNames.states) {
                    console.log('transaction');
                    var transaction = database.transaction(['states'], 'readwrite');

                    transaction.oncomplete = function(e) {
                        var objectStore = transaction.objectStore('states');
                        var request = objectStore.get(0);

                        request.onsuccess = function(e) {
                            resolve(e.target.result);
                        };

                        request.onerror = function(e) {
                            reject();
                        };
                    };

                    transaction.onerror = function(e) {
                        reject('transaction error');
                    };
                } else {
                    reject('No database states present');
                }
            }, function(err) {
                console.warn(err);
            });
        });
    };



    // Set
    var set = function(data, callback) {
        return new Promise(function(resolve, reject) {
            var start = performance.now();

            var transaction = database.transaction(['states'], 'readwrite');
            var objectStore = transaction.objectStore('states');
            var request = objectStore.put(data, 0);

            request.onsuccess = function(e) {
                console.log('[' + /\d\d\:\d\d\:\d\d/.exec(new Date())[0] + ']', 'Saved state to IndexedDB. ' + (performance.now() - start).toFixed(2) + 'ms');
                resolve();
            };

            request.onerror = function(e) {
                reject();
            };
        });
    };



    // Clear
    var clear = function() {
        return new Promise(function(resolve, reject) {
            var transaction = database.transaction(['states'], 'readwrite');
            var objectStore = transaction.objectStore('states');
            var request = objectStore.clear();

            request.onsuccess = function(e) {
                console.log('[' + /\d\d\:\d\d\:\d\d/.exec(new Date())[0] + ']', 'Cleared IndexedDB.');
                resolve();
            };

            request.onerror = function(e) {
                reject();
            };
        });
    };



    // Init
    var init = function() {
        saveBtn = document.getElementById('save');
        clearBtn = document.getElementById('clear');

        if (indexedDB === undefined) {
            console.warn('Storage: IndexedDB not available.');
        }
    }();
};
