/* ==========================================================================
   SolGod - SOL
   ========================================================================== */

solGod.solElements = function(undefined) {

    var exports = this.solElements;



    // Object
    exports.objectBody = function(x, y, z) {
        this.r = 0;
        this.x = x;
        this.y = y;
        this.z = z;
        this.geometry = [];
        this.material = '';
        this.mesh = '';
        this.vertexArr = [];
        this.vertexDeg = [];
        this.vertexWaveCoe = 0;
        this.vertexDegCoe = 4;
    };

    exports.objectBody.prototype.init = function(geometry, material, vertexDegCoe) {
        this.geometry = geometry;
        this.material = material;
        this.vertexDegCoe = vertexDegCoe;
        this.mesh = new THREE.Mesh(this.geometry, this.material);
        this.r = this.geometry.parameters.radius;
        this.vertexWaveCoe = this.r / 30;

        this.geometry.mergeVertices();
        this.updateVerticesInit();
        this.setPosition();

        this.mesh.rotation.set(solGod.get.radian(10), 0, 0);
        this.mesh.receiveShadow = true;
    };

    exports.objectBody.prototype.setPosition = function() {
        this.mesh.position.set(this.x, this.y, this.z);
    };

    exports.objectBody.prototype.updateVerticesInit = function() {
        if (this.mesh.geometry.faces.length > 0 && this.mesh.geometry.vertices.length > 0) {
            var vertices = this.mesh.geometry.vertices;

            for (var i = 0; i < vertices.length; i++) {
                var r = this.r;
                this.vertexArr[i] = r;
                this.vertexDeg[i] = solGod.get.randomInt(0, 360);
                vertices[i].normalize().multiplyScalar(r);
            }

            this.mesh.geometry.computeVertexNormals();
            this.mesh.geometry.computeFaceNormals();
            this.mesh.geometry.verticesNeedUpdate = true;
            this.mesh.geometry.elementsNeedUpdate = true;
            this.mesh.geometry.normalsNeedUpdate = true;
        }
    };

    exports.objectBody.prototype.updateVertices = function() {
        if (this.mesh.geometry.faces.length > 0 && this.mesh.geometry.vertices.length > 0) {
            var vertices = this.mesh.geometry.vertices;

            for (var i = 0; i < this.vertexArr.length; i++) {
                var r;
                this.vertexDeg[i] += this.vertexDegCoe;
                r = this.vertexArr[i] + Math.sin(solGod.get.radian(this.vertexDeg[i])) * this.vertexWaveCoe;
                vertices[i].normalize().multiplyScalar(r);
            }

            this.mesh.geometry.computeVertexNormals();
            this.mesh.geometry.computeFaceNormals();
            this.mesh.geometry.verticesNeedUpdate = true;
            this.mesh.geometry.elementsNeedUpdate = true;
            this.mesh.geometry.normalsNeedUpdate = true;
        }
    };

    exports.objectBody.prototype.updateColor = function(color, colorReflect) {
        this.mesh.material.color.set(color);
        this.mesh.material.specular.set(colorReflect);
        this.mesh.material.emissive.set(colorReflect);
    };
};
