/* ==========================================================================
   SolGod - Lights
   ========================================================================== */

solGod.lights = function(undefined) {

    var exports = this.lights;


    // Ambient Light
    exports.ambientLight = function() {
        this.color = '0x663344';
        this.obj = '';
    };

    exports.ambientLight.prototype.init = function(scene, hex1) {
        this.color = hex1;
        this.obj = new THREE.AmbientLight(hex1);
        scene.add(this.obj);
    };



    // Flare
    exports.flare = function() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.h = 0;
        this.s = 0;
        this.l = 0.6;
        this.obj = '';
    };

    exports.flare.prototype.init = function(scene, x, y, z) {
        // Save defaults
        this.x = x;
        this.y = y;
        this.z = z;

        // Group
        this.obj = new THREE.Object3D();

        // Load textures
        var textureLoader = new THREE.TextureLoader();
        var textureFlare0 = textureLoader.load('dist/img/lensflare0.png');
        var textureFlare2 = textureLoader.load('dist/img/lensflare2.png');
        var textureFlare3 = textureLoader.load('dist/img/lensflare3.png');

        // Add point light
        var light = new THREE.PointLight(0xffffff, 1.5, 2000);
        light.color.setHSL(this.h, this.s, this.l);
        light.position.set(this.x, this.y, this.z);

        this.obj.add(light);

        // Add flare
        var flareColor = new THREE.Color(0xffffff);
        flareColor.setHSL(this.h, this.s, this.l + 0.5);

        var lensFlare = new THREE.LensFlare(textureFlare0, 700, 0.0, THREE.AdditiveBlending, flareColor);
        lensFlare.add(textureFlare2, 512, 0.0, THREE.AdditiveBlending);
        lensFlare.add(textureFlare2, 512, 0.0, THREE.AdditiveBlending);
        lensFlare.add(textureFlare2, 512, 0.0, THREE.AdditiveBlending);
        lensFlare.add(textureFlare3, 60, 0.6, THREE.AdditiveBlending);
        lensFlare.add(textureFlare3, 70, 0.7, THREE.AdditiveBlending);
        lensFlare.add(textureFlare3, 120, 0.9, THREE.AdditiveBlending);
        lensFlare.add(textureFlare3, 70, 1.0, THREE.AdditiveBlending);
        lensFlare.customUpdateCallback = this.updateCallback;
        lensFlare.position.copy(light.position);

        this.obj.add(lensFlare);

        scene.add(this.obj);
    };

    exports.flare.prototype.updateCallback = function(object) {
        var f, fl = object.lensFlares.length;
        var flare;
        var vecX = -object.positionScreen.x * 2;
        var vecY = -object.positionScreen.y * 2;
        for( f = 0; f < fl; f++ ) {
            flare = object.lensFlares[ f ];
            flare.x = object.positionScreen.x + vecX * flare.distance;
            flare.y = object.positionScreen.y + vecY * flare.distance;
            flare.rotation = 0;
        }
        object.lensFlares[ 2 ].y += 0.025;
        object.lensFlares[ 3 ].rotation = object.positionScreen.x * 0.5 + THREE.Math.degToRad( 45 );
    };
};
