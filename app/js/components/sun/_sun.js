/* ==========================================================================
   SolGod - Sun
   ========================================================================== */

solGod.sun = function(scene, undefined) {

    var exports = this.sun;

    var sun;

    var ambientLight, flare;

    var sunX = 400,
        sunY = 400,
        sunZ = 400;



    // Emit light
    var emitLight = function() {
        solGod.lights();

        // Ambient Light
        ambientLight = new solGod.lights.ambientLight();
        ambientLight.init(scene, 0x404040);

        // Flare
        flare = new solGod.lights.flare();
        flare.init(scene, sunX, sunY, sunZ);
    };



    // Init
    var init = function() {
        emitLight();
    }();
};
