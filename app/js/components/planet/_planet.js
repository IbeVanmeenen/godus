/* ==========================================================================
   SolGod - Planet
   ========================================================================== */

solGod.planet = function(scene, undefined) {

    var exports = this.planet;

    var creation, planet, land, troposphere;

    var planetX = 0,
        planetY = 0,
        planetZ = 0;

    var planetStartRadius = 2;
    var planetRadius = planetStartRadius;
    var planetMaxRadius = 25;
    var currentVertexDegCoePlanet = 4;
    var vertexDegCoeMinPlanet = 0;

    var hasLand = false;
    var landStartScale = 15;
    var landScale = landStartScale;
    var landMaxScale = 19.2;

    var hasClouds = false;

    var colorArray = [];
    var colorReflectArray = [];
    var colorRange = 1000;
    var currentColorIndex = 0;

    var hasPlanet = false;



    // Global creation
    var addCreation = function() {
        creation = new THREE.Object3D();
    };



    // Planet
    var addPlanet = function() {
        // Get color aray
        colorArray = solGod.get.colorArray('#ff5f2e', '#80d4f6', colorRange);
        colorReflectArray = solGod.get.colorArray('#ff5f2e' ,'#282c37', colorRange);

        // Make Planet
        var radius = planetRadius,
            segments = 9,
            rings = 7;

        var startColor = '#ff5f2e',
            startSpecEmi = '#ff5f2e';

        if (globTest) {
            startColor = '#80d4f6';
            startSpecEmi = '#282c37';
        }

        solGod.save.getData().then(function(data) {
            var loader = new THREE.ObjectLoader();

            loader.load(data.planet, function (planet) {
                planet = data.planet;

                // Add to global
                creation.add(planet.mesh);

                hasPlanet = true;
            });
        }, function() {
            var planetGeometry = new THREE.SphereGeometry(radius, segments, rings);

            var planetMaterial = new THREE.MeshPhongMaterial({
                color: startColor,
                shininess: 2,
                specular: startSpecEmi,
                emissive: startSpecEmi,
                shading: THREE.FlatShading
            });

            planet = new solGod.solElements.objectBody(planetX, planetY, planetX);
            planet.init(planetGeometry, planetMaterial, currentVertexDegCoePlanet);

            // Add to global
            creation.add(planet.mesh);

            hasPlanet = true;
        });
    };


    // Get planet
    exports.getPlanet = function() {
        return planet;
    };



    // Add Troposphere (where the clouds live)
    var addTroposphere = function() {
        // Create group
        troposphere = new THREE.Object3D();

        // Add to global
        creation.add(troposphere);
    };



    // Add land
    var addLand = function() {
        // Make Land
        var radius = landStartScale,
            detail = 4;

        var landGeometry = new THREE.TetrahedronGeometry(radius, detail);

        var landMaterial = new THREE.MeshPhongMaterial({
            color: solGod.get.landColor(),
            shininess: 0,
            specular: 0x404040,
            emissive: 0x404040,
            shading: THREE.FlatShading
        });

        land = new solGod.planetElements.land();
        land.init(landGeometry, landMaterial);

        // Add to global
        creation.add(land.mesh);
    };



    // Add mountains
    var addMountains = function() {

    };



    // Add Forest El
    var addForestEl = function() {
        var radiusTop = 0,
            radiusBottom = 2,
            height = 4,
            radiusSegments = 4,
            heightSegments = 1;

        var forestGeometry = new THREE.CylinderGeometry(radiusTop, radiusBottom, height, radiusSegments, heightSegments);
        var forestMaterial = new THREE.MeshPhongMaterial({
            color: '#8cd790',
            shininess: 0,
            specular: 0x404040,
            emissive: 0x404040,
            shading: THREE.FlatShading
        });

        var forestEl = new solGod.planetElements.forest();
        forestEl.init(forestGeometry, forestMaterial, planetStartRadius * planetMaxRadius);

        // Add to global
        creation.add(forestEl.stick);
    };

    var addForest = function() {
        // Add first Three
        addForestEl();

        // Add all threes
        var _delayedAdd = function() {
            setTimeout(function() {
                addForestEl();
            }, solGod.get.randomInt(100, 10000));
        };

        for (var i = 0; i < solGod.get.randomInt(10, 100); i++) {
            _delayedAdd();
        }
    };



    // Add Clouds
    var addCloudEl = function() {
        var cloudGeometry = new THREE.CubeGeometry(6, 5, solGod.get.randomInt(1, 2));
        var cloudMaterial = new THREE.MeshPhongMaterial({
            color: 0xffffff,
            shininess: 0,
            specular: 0xffffff,
            emissive: 0xffffff,
            transparent: true,
            opacity: 0.8,
            shading: THREE.FlatShading
        });

        var cloud = new solGod.planetElements.cloud();
        cloud.init(cloudGeometry, cloudMaterial, planetStartRadius * planetMaxRadius);

        // Add stick to Troposphere
        troposphere.add(cloud.stick);
    };

    var addClouds = function() {
        // Add first cloud
        addCloudEl();

        var _delayedAdd = function() {
            setTimeout(function() {
                addCloudEl();
            }, solGod.get.randomInt(100, 5000));
        };

        for (var i = 0; i < solGod.get.randomInt(10, 50); i++) {
            _delayedAdd();
        }
    };



    // Grow
    var growPlanet = function() {
        // Grow size
        if (planetRadius < planetMaxRadius) {
            planetRadius += 0.01;
            planet.mesh.scale.x += 0.01;
            planet.mesh.scale.y += 0.01;
            planet.mesh.scale.z += 0.01;
        }


        // Add Land
        if (!hasLand && planetRadius > 20) {
            hasLand = true;
            addLand();
        }

        // Land size
        if (hasLand && landScale < landMaxScale) {
            landScale += 0.005;
            land.mesh.scale.x += 0.0025;
            land.mesh.scale.y += 0.0025;
            land.mesh.scale.z += 0.0025;
        }


        // Update Tectonic Plates
        if (planetRadius > 9 && planetRadius < 15 && currentVertexDegCoePlanet > 0) {
            currentVertexDegCoePlanet -= 0.01;
            planet.vertexDegCoe -= 0.01;
        }
        if (planetRadius > 15 && planetRadius < 19 && currentVertexDegCoePlanet < 1) {
            console.log('increase');
            currentVertexDegCoePlanet += 0.01;
            planet.vertexDegCoe += 0.01;
        }

        if (landScale >= landMaxScale && currentVertexDegCoePlanet > vertexDegCoeMinPlanet) {
            console.log('decrease');
            currentVertexDegCoePlanet -= 0.05;
            planet.vertexDegCoe -= 0.05;
        }


        // Update color Lava -> Sea
        if (planetRadius > 10 && currentColorIndex < colorRange) {
            currentColorIndex ++;
            planet.updateColor(colorArray[currentColorIndex], colorReflectArray[currentColorIndex]);
        }


        // Add clouds
        if (landScale >= landMaxScale && !hasClouds) {
            hasClouds = true;
            addClouds();
        }


        // Update Age
        solGod.sol.updateAge();
    };


    // render Update elements
    exports.renderUpdate = function() {
        creation.rotation.x += 0.0001; // Let planet rotate
        creation.rotation.y += 0.0001;

        troposphere.rotation.x -= 0.0001;
        troposphere.rotation.y -= 0.0001;

        if (hasPlanet) {
            planet.updateVertices();
        }

        if (globGrow && !globTest) {
            growPlanet();
        }
    };


    // Grow on click
    var growOnHold = function() {
        // Space Up
        document.onkeyup = function(e) {
            e = window.event || e;

            if (e.keyCode === 32) {
                e.preventDefault();
                globGrow = false;
            }
        };

        // Space Down
        document.onkeydown = function(e) {
            e = window.event || e;

            if (e.keyCode === 32) {
                e.preventDefault();
                globGrow = true;
            }
        };
    };


    // Init
    var init = function() {
        solGod.planetElements();

        addCreation();
        addPlanet();
        addTroposphere();

        scene.add(creation);

        growOnHold();

        if (globTest) {
            // Set Planet
            for (var i = 0; i < (planetMaxRadius - planetStartRadius); i += 0.01) {
                planet.mesh.scale.x += 0.01;
                planet.mesh.scale.y += 0.01;
                planet.mesh.scale.z += 0.01;

                planet.vertexDegCoe = vertexDegCoeMinPlanet;
            }

            // Set Land
            addLand();
            for (var j = 0; j < (landMaxScale - landStartScale); j += 0.01) {
                land.mesh.scale.x += 0.005;
                land.mesh.scale.y += 0.005;
                land.mesh.scale.z += 0.005;
            }

            // Add clouds
            for (var k = 0; k < solGod.get.randomInt(10, 50); k++) {
                addCloudEl();
            }

            // Add Threes
            // for (var l = 0; l < solGod.get.randomInt(10, 200); l++) {
                // addForestEl();
            // }
        }
    }();
};
