/* ==========================================================================
   SolGod - Planet Elements
   ========================================================================== */

solGod.planetElements = function(undefined) {

    var exports = this.planetElements;



    // Land
    exports.land = function() {
        this.r = 0;
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.geometry = [];
        this.material = '';
        this.mesh = '';
    };

    exports.land.prototype.init = function(geometry, material) {
        this.geometry = geometry;
        this.material = material;

        // Update geometry to less perfect circle
        var noise = 1;

        for (var i = 0; i < this.geometry.vertices.length; i++) {
            var v = this.geometry.vertices[i];

            if (i > 20 && i < 120) {
                v.x += 0;
                v.y += 0;
                v.z += -2;
            } else {
                v.x += -noise / 10 + Math.random() * noise;
                v.y += -noise / 10 + Math.random() * noise;
                v.z += -noise / 10 + Math.random() * noise;
            }

        }

        this.mesh = new THREE.Mesh(geometry, material);
        this.mesh.receiveShadow = true;
        this.mesh.castShadow = true;
    };



    // Cloud
    exports.cloud = function() {
        this.x = 0;
        this.y = 0;
        this.z = 42;
        this.geometry = [];
        this.material = '';
        this.radius = 4;
        this.detail = 2;
        this.stick = '';
        this.point = '';
        this.cloud = '';
    };

    exports.cloud.prototype.init = function(geometry, material, planetMaxRadius) {
        // Store
        this.geometry = geometry;
        this.material = material;
        this.z = planetMaxRadius;

        // Stick & Point
        this.stick = new THREE.Object3D();
        this.point = new THREE.Vector3(0, 0, 0);
        this.stick.lookAt(this.point);

        // Cloud
        this.cloud = new THREE.Mesh(this.geometry, this.material);
        this.cloud.receiveShadow = true;
        this.cloud.castShadow = true;

        // Add to stick
        this.stick.add(this.cloud);

        // Set positions
        this.setPosition();
    };

    exports.cloud.prototype.setPosition = function() {
        // Set mesh position
        this.cloud.position.set(this.x, this.y, this.z);
        this.cloud.lookAt(this.point);

        // Set random position around le troposphere
        this.stick.rotation.y = solGod.get.randomInt(0, 360);
        this.stick.rotation.x = solGod.get.randomInt(0, 360);
    };



    // Forest
    exports.forest = function() {
        this.x = 0;
        this.y = 0;
        this.z = 42;
        this.geometry = [];
        this.material = '';
        this.radius = 4;
        this.detail = 2;
        this.stick = '';
        this.point = '';
        this.forest = '';
    };

    exports.forest.prototype.init = function(geometry, material, planetMaxRadius) {
        // Store
        this.geometry = geometry;
        this.material = material;
        // this.z = planetMaxRadius + 10;
        this.z = planetMaxRadius - 1;

        // Stick & Point
        this.stick = new THREE.Object3D();
        this.point = new THREE.Vector3(0, 0, 0);
        this.stick.lookAt(this.point);

        // Forest
        this.geometry.rotateX(-Math.PI / 2);
        this.forest = new THREE.Mesh(this.geometry, this.material);
        this.forest.receiveShadow = true;
        this.forest.castShadow = true;

        // Add to stick
        this.stick.add(this.forest);

        // Set positions
        this.setPosition();
    };

    exports.forest.prototype.setPosition = function() {
        // Set mesh position
        this.forest.position.set(this.x, this.y, this.z);
        this.forest.lookAt(this.point);

        // Set random position around le troposphere
        this.stick.rotation.y = solGod.get.randomInt(0, 360);
        this.stick.rotation.x = solGod.get.randomInt(0, 360);
    };
};
