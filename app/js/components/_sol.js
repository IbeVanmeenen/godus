/* ==========================================================================
   SolGod - SOL
   ========================================================================== */

solGod.sol = function(undefined) {

    var exports = this.sol;

    var container;
    var windowWidth, windowHeight;
    var camAngle, camAspect, camNear, camFar;
    var renderer, camera, scene;
    var controls;

    var sol;

    var age = 0;
    var ageEl;



    // Planet
    var addPlanet = function() {
        solGod.planet(sol);
    };



    // Sun
    var addSun = function() {
        solGod.sun(sol);
    };



    // Age Setup
    var setupAge = function() {
        // solGod.save.getData().then(function(data) {
        //     age = parseInt(data.age, 10);
        //     ageEl.innerHTML = age;
        //
        //     updateAge();
        // }, function() {
            age = 0;

            updateAge();
        // });
    };



    // Age Update
    var updateAge = function() {
        age += 100;
        ageEl.innerHTML = age;

        setTimeout(updateAge, 1000);
    };



    // Extra Age update
    exports.updateAge = function() {
        age += (1000 + solGod.get.randomInt(1, 999));
        ageEl.innerHTML = age;
    };



    // Setup Three World
    var setup = function() {
        // Activate Sol elements
        solGod.solElements();

        // Create renderer (WebGL), camera and scene
        renderer = new THREE.WebGLRenderer({
            alpha: true,
            antialias: true
        });
        camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, globMaxZoom);
        scene = new THREE.Scene();

        // Add camera to scene
        scene.add(camera);

        // Camera Position
        camera.position.z = 300;

        // Enable shadow map
        renderer.shadowMap.enabled = true;

        // Set render size
        renderer.setSize(windowWidth, windowHeight);

        // Attach to DOM
        container.appendChild(renderer.domElement);

        // Create solar system
        sol = new THREE.Object3D();

        // Add camera to scene
        scene.add(sol);

        // Add Sun
        addSun();

        // Add Planet
        addPlanet();

        // Start render loop
        render();

        // Set controls
        controls = new THREE.OrbitControls(camera, renderer.domElement);

        // Fixed interval planet age
        setupAge();

        // Setup Save actions
        solGod.save.setupSave(ageEl);
    };



    // Render
    var render = function() {
        solGod.planet.renderUpdate();

        renderer.render(scene, camera);

        requestAnimationFrame(render);
    };



    // Init
    var init = function() {
        windowWidth = window.innerWidth;
        windowHeight = window.innerHeight;

        camAngle = 45;
        camAspect = windowWidth / windowHeight;
        camNear = 0.1;
        camFar = 10000;

        container = document.getElementById('world');
        ageEl = document.getElementById('world-age');

        setup();
    }();
};
